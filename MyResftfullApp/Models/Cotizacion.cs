﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResftfullApp.Models
{
    public class Cotizacion
    {
        public Cotizacion(decimal compra, decimal venta, string fechaActualizacion)
        {
            this.Compra = compra;
            this.Venta = venta;
            this.FechaActualizacion = fechaActualizacion;
        }

        public decimal Compra { get; set; }
        public decimal Venta { get; set; }
        public string FechaActualizacion { get; set; }
    }
}