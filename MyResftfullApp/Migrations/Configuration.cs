﻿using MyResftfullApp.Data;
using MyResftfullApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace MyResftfullApp.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<MyAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MyAppContext context)
        {
            context.Users.AddOrUpdate(x => x.ID,
                new User() { ID = 1, Nombre = "Nelson", Apellido = "Garrido", Email = "nelson.garrido@live.com.ar", Password = "Clave1" }
                );
        }
    }
}