﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MyResftfullApp.Data
{
    public class MyAppContext : DbContext
    {
        public MyAppContext() : base("MyRestfullAppConnectionString")
        {
   
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }



        public System.Data.Entity.DbSet<Models.User> Users { get; set; }
    }
}