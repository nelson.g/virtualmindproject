﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyResftfullApp.Models;

namespace MyResftfullApp.Data
{
    public class Repository : IRepository
    {
        MyAppContext _db = new MyAppContext();

        public IEnumerable<User> GetUsers()
        {
            return _db.Users.ToList();
        }

        public User InsertUser(User usuario)
        {
            _db.Users.Add(usuario);
            _db.SaveChanges();
            return usuario;
        }

        public User UpdateUser(User usuario)
        {
            var user = _db.Users.FirstOrDefault(f => f.ID == usuario.ID);
            if (user != null)
            {
                user.Nombre = usuario.Nombre;
                user.Apellido = usuario.Apellido;
                user.Email = usuario.Email;
                user.Password = usuario.Password;

                _db.SaveChanges();
                return user;
            }
            return null;
        }

        public int DeleteUser(int id)
        {
            var user = _db.Users.FirstOrDefault(f => f.ID == id);
            if (user != null)
            {
                _db.Users.Remove(user);
                return _db.SaveChanges();
            }
            return 0;
        }
    }
}