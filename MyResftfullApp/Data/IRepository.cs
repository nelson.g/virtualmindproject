﻿using MyResftfullApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyResftfullApp.Data
{
    interface IRepository
    {
        IEnumerable<User> GetUsers();
        User InsertUser(User usuario);
        User UpdateUser(User usuario);
        int DeleteUser(int id);
    }
}
