﻿using MyResftfullApp.Data;
using MyResftfullApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyResftfullApp.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        public IEnumerable<Models.User> Get()
        {
            return new Repository().GetUsers();
        }

        [HttpPost]
        public User Insert([FromBody] User usuario)
        {
            if (ModelState.IsValid)
            {
                return new Repository().InsertUser(usuario);
            }
            return null;
        }

        [HttpPut]
        public User Update([FromBody] User usuario)
        {
            if (ModelState.IsValid)
            {
                return new Repository().UpdateUser(usuario);
            }
            return null;
        }

        [HttpDelete]
        public int Delete(int id)
        {
            return new Repository().DeleteUser(id);
        }
    }
}
