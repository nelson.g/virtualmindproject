﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using System.Web.Http;

namespace MyResftfullApp.Controllers
{
    public class CotizacionController : ApiController
    {


        [HttpGet]
        public Models.Cotizacion Get(string moneda)
        {
            Strategy.Context context;

            switch (moneda.ToLower())
            {
                case "dolar":
                    context = new Strategy.Context(new Strategy.DolarStrategy());
                    return context.GetCotizacion();
                case "pesos":
                    context = new Strategy.Context(new Strategy.PesoStrategy());
                    return context.GetCotizacion();
                case "real":
                    context = new Strategy.Context(new Strategy.RealStrategy());
                    return context.GetCotizacion();
            }

            return null;
        }

    }
}

