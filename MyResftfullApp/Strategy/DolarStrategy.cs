﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using MyResftfullApp.Models;

namespace MyResftfullApp.Strategy
{
    public class DolarStrategy : IStrategy
    {
        public Cotizacion GetCotizacion()
        {
            var url = "https://www.bancoprovincia.com.ar/Principal/Dolar";
            var dataBanca = new WebClient().DownloadString(url);
            string[] data = dataBanca.Split('"');
            return new Cotizacion(Convert.ToDecimal(data[1]), Convert.ToDecimal(data[3]), Convert.ToString(data[5]));
        }
    }
}