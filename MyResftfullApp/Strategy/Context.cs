﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResftfullApp.Strategy
{
    public class Context
    {
        private IStrategy _strategy;

        public Context(IStrategy strategy)
        {
            this._strategy = strategy;
        }

        public Models.Cotizacion GetCotizacion()
        {
            return _strategy.GetCotizacion();
        }
    }
}