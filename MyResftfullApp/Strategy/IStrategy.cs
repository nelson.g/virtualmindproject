﻿using MyResftfullApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyResftfullApp.Strategy
{
    public interface IStrategy
    {
        Cotizacion GetCotizacion();
    }
}
