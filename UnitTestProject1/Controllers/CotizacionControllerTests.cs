﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyResftfullApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyResftfullApp.Controllers.Tests
{
    [TestClass()]
    public class CotizacionControllerTests
    {
        [TestMethod()]
        public void GetTest()
        {
            // Arrange
            var controller = new CotizacionController();

            // Act
            var response = controller.Get("dolar");

            // Asserts
            // Check For Nulls
            Assert.IsNotNull(response);

            // Check Response Type
            Assert.AreEqual(typeof(Models.Cotizacion), response.GetType());
        }
    }
}